#include <iostream> 
#include <iomanip> 
#include <cmath>
#include <time.h>
using namespace std;

int main()
{
    srand(time(NULL));
    const int n = 1000;
    int array[n];

    for (int i = 0; i < n; i++) {     //Заполнение массива на 1000 элементов с случайными 
        array[i] = rand() % 100 + 1;  //значениями 1-100.
        cout << array[i] << " ";
    }

    //int k;
    //while(k < n)           //Альтернативное заполнение массива на 1000 элементов с 
    //{                     //случайными значениями 20-60 для проверки однородности ряда.
    //    array[k] = rand()%100+1; 
    //    if (array[k] < 20 || array[k] > 60) k--;
    //    k++;
        //cout << array[k] << " ";
    //}

    /*Средняя арифметическая*/
    float avg = 0;
    for (int i = 0; i < n; i++)
        avg += array[i];

    avg = avg / n;
    cout << endl << "Средняя арифметическая: " << avg << endl;
    /*Средняя арифметическая*/


    /*Среднее квадратическое и Среднеквадратическое отклонение*/
    float   sqravg,
        stddev,
        sumsqavg = 0,
        sumstddev = 0;
    for (int i = 0; i < n; i++) {
        sumsqavg += pow(array[i], 2);
        sumstddev += pow((array[i] - avg), 2);
    }
    sqravg = sqrt(sumsqavg / n);
    stddev = sqrt(sumstddev / n);

    cout << "Среднее квадратическое: " << sqravg << endl;
    cout << "Среднеквадратическое отклонение: " << stddev << endl;
    /*Среднее квадратическое  и Среднеквадратическое отклонение*/

    /*Нахождение медианы (как середины отсортированного массива)*/
    int tmp;
    for (int i = 0; i < n - 1; i++)     //Сортировка элементо по возрастанию
        for (int j = 0; j < n - i - 1; j++)
            if (array[j] > array[j + 1]) {
                tmp = array[j]; array[j] = array[j + 1]; array[j + 1] = tmp;
            }

    int median;
    if (n % 2 == 0)
        median = (array[n / 2 - 1] + array[(n / 2)]) / 2;
    else if (n % 2 != 0)
        median = array[(n + 1) / 2 - 1];

    cout << "Медиана: " << median << endl;
    /*Нахождение медианы*/


    /*Нахождение средней арифметической взвешенной*/
    int countarray[5];
    int cnt = 0;
    int primedigit = 1, seconddigit = 19;
    for (int z = 0; z < 5; z++)
    {
        for (int i = 0; i < n; i++) {
            tmp = array[i];
            if (tmp >= primedigit && tmp <= seconddigit)    cnt++;
        }
        countarray[z] = cnt;
        cnt = 0;
        cout << "Повтор в интервале [" << primedigit << "," << seconddigit << "]: " << countarray[z] << endl;
        if (primedigit == 1) primedigit = 0;
        primedigit += 20;
        seconddigit += 20;
        if (z == 3) seconddigit += 1;
    }

    float avgwegh = 0;
    int avgdigit = 10;
    for (int i = 0; i < 5; i++) {
        avgwegh += countarray[i] * avgdigit;
        avgdigit += 20;
    }

    avgwegh = avgwegh / n;

    cout << "Среднее взвешенное: " << avgwegh << endl << endl;
    /*Нахождение средней арифметической взвешенной*/



    /*Нахождение медианного интервала и заодно медианы (по формуле)*/
    int accumfreqarray[5];
    int accum = 0;
    for (int i = 0; i < 5; i++) {
        accum += countarray[i];
        accumfreqarray[i] = accum;
    }

    for (int i = 0; i < 5; i++)
        cout << accumfreqarray[i] << endl;  //Массив накопленных частот по интервалам

    int neededprevacfreq,
        neededcurindex,     // Индекс медианного интервала
        neededprimedigit,   // Первое число медианного интервала
        difference;
    float medianbyformula;

    float h = accumfreqarray[4] / 2;      //Половина накопленных частот
    for (int i = 0; i < 5; i++)         //Зная это находим медианный интевал (он должен 
        if (accumfreqarray[i] < h) {    //быть больше этого значения)
            neededprevacfreq = accumfreqarray[i];
            neededcurindex = i + 1;     // Индекс медианного интервала
        }                             // Поможет сопоставить интервал и частоты в массиве

    switch (neededcurindex) {
    case 1: neededprimedigit = 20;
        difference = 19;
        break;
    case 2: neededprimedigit = 40;
        difference = 19;
        break;
    case 3: neededprimedigit = 60;
        difference = 19;
        break;
    case 4: neededprimedigit = 80;
        difference = 20;
        break;
    default: neededprimedigit = 0;
        difference = 19;
    }

    cout << "Половина накопленных частот: " << h << endl;
    cout << "Медианный интервал: [" << neededprimedigit << ", " << neededprimedigit + difference << "]" << endl;

    medianbyformula = neededprimedigit + difference * ((h - neededprevacfreq) / countarray[neededcurindex]);

    cout << "Медиана (по формуле): " << medianbyformula << endl;
    /*Нахождение медианного интервала и заодно медианы (по формуле)*/

    cout << "\n////////////////Массив отсортированный по значениям\n";

    /*Формирование двумерно массива (2 x n) элемент и ко-во раз он повторялся (без повторений элементов в массиве)*/
    int** darray = new int* [2];
    for (int i = 0; i < 2; ++i) //Создание динамического двумерного массива (2 x n)
        darray[i] = new int[n];

    int count = 0;

    for (int i = 0; i < n; i++) {     //Заполнение массива darray[][]
        darray[0][i] = array[i];
        for (int j = 0; j < n; j++)
            if (darray[0][i] == array[j]) count++;
        darray[1][i] = count;
        count = 0;
    }

    //for (int i = 0; i < n; i++)       //Неотсортированный оригинальный массив
    //    cout << darray[0][i] << setw(4) << darray[1][i] << endl;

    for (int i = 0; i < n - 1; i++)     //Сортировка двумерного массива по возрастанию по значениям  
        for (int j = 0; j < n - i - 1; j++)
           if (darray[0][j] > darray[0][j + 1])
           {
             tmp = darray[1][j]; darray[1][j] = darray[1][j + 1]; darray[1][j + 1] = tmp;
             tmp = darray[0][j]; darray[0][j] = darray[0][j + 1]; darray[0][j + 1] = tmp;
           }



    int** tmpdarray = new int* [2];     //Создание второго динамического массива (2 x n)
    for (int i = 0; i < 2; ++i)
        tmpdarray[i] = new int[n];

    for (int i = 0; i < n; i++) {   //Заполнение нулями
        tmpdarray[0][i] = 0;
        tmpdarray[1][i] = 0;
    }

    int q = 0;                       //Формирование массива из неповторяющихся элементов
    for (int i = 0; i < n; i++) {
        tmpdarray[0][q] = darray[0][i];
        tmpdarray[1][q] = darray[1][i];

        if (darray[0][i + 1] == darray[0][i]) continue;
        q++;
    }

    int newsize = 0;                    //Новый размер массива
    for (int i = 0; ; i++) {
        if (tmpdarray[1][i] == 0) break;
        newsize++;
    }


    int** sdarray = new int* [2];       //Создание динамического двумерного массива 
    for (int i = 0; i < 2; ++i)         //(готового) с размером newsize и переписывание
        sdarray[i] = new int[newsize];  // в него всех элементов из tmpdarray


    for (int i = 0; i < newsize; i++) { 

        sdarray[0][i] = tmpdarray[0][i];
        sdarray[1][i] = tmpdarray[1][i];
    }

                                for (int i = 0; i < 2; i++)     //Удаление darray[2][n]
                                    delete[] darray[i];
                                delete[] darray;
                                darray = 0;


                                for (int i = 0; i < 2; i++)   //Удаление tmpdarray[2][n]
                                    delete[] tmpdarray[i];
                                delete[] tmpdarray;
                                tmpdarray = 0;

    for (int i = 0; i < newsize; i++) {
        cout << sdarray[0][i] << setw(4) << sdarray[1][i] << endl;
    }
    /*Формирование двумерно массива (2 x n) элемент и ко-во раз он
    повторялся (без повторений элементов в массиве)*/

    cout << "\n\n";

    /*Частота варианты равной 10, Размах ряда, Коэфициент осциляции ряда, Коэфициент вариации ряда, Однородность*/
    for (int i = 0; i < newsize; i++)
        if (sdarray[0][i] == 10) {
            cout << "Частота варианты равной 10: " << sdarray[1][i] << endl;
            break;
        }

    int range = sdarray[0][newsize - 1] - sdarray[0][0];
    cout << "Размах ряда: " << range << endl;

    cout << "Коэффициент осцилляции ряда : " << range / avg << endl;

    cout << "Коэффициент вариации ряда: " << stddev / avg;
    if (stddev / avg > 0.33) cout << "  > 0.33 --> Ряд неоднородный" << endl;
    else if (stddev / avg <= 0.33) cout << "  < 0.33 --> Ряд однородный" << endl;
    /*Частота варианты равной 10, Размах ряда, Коэфициент осциляции ряда, Коэфициент вариации ряда, Однородность*/


    /*Формирование двумерного массива (3 x newsize) для кумуляты ряда*/
    int** triparray = new int* [3];
    for (int i = 0; i < 3; ++i)       //третья строка - накопленные частоты по элементам
        triparray[i] = new int[newsize];

    int accumfreq = 0;
    for (int i = 0; i < newsize; i++) {
        triparray[0][i] = sdarray[0][i];
        triparray[1][i] = sdarray[1][i];

        accumfreq += sdarray[1][i];

        triparray[2][i] = accumfreq;
    }
    /*Формирование двумерного массива (3 x newsize) для кумуляты ряда*/



    /*Самый частовстречающийся элемент*/
    for (int i = 0; i < newsize - 1; i++)
      for (int j = 0; j < newsize - i - 1; j++)   //Сортировка двумерного массива по возрастанию по повторениям
        if (sdarray[1][j] > sdarray[1][j + 1]) 
        {
         tmp = sdarray[1][j]; sdarray[1][j] = sdarray[1][j + 1]; sdarray[1][j + 1] = tmp;
         tmp = sdarray[0][j]; sdarray[0][j] = sdarray[0][j + 1]; sdarray[0][j + 1] = tmp;
        }

    cout << "Самый часто встречающийся элемент: " << sdarray[0][newsize - 1] << ", встречется " << sdarray[1][newsize - 1] << " раз" << endl;
    cout << "Второй часто встречающийся элемент: " << sdarray[0][newsize - 2] << ", встречется " << sdarray[1][newsize - 2] << " раз" << endl;
    /*Самый частовстречающийся элемент*/

    cout << "\n\n/////////////////Массив отсортированный по повторениям\n";
    for (int i = 0; i < newsize; i++)
        cout << sdarray[0][i] << setw(4) << sdarray[1][i] << endl;
    cout << "\n\n";

    cout << "\n\n/////////////////Массив (3 x newsize) с накопленными частотами по элементам (для кумуляты)\n";
    for (int i = 0; i < newsize; i++)
        cout << triparray[0][i] << setw(4) << triparray[1][i] << setw(4) << triparray[2][i] << endl;


    cout << "\n\n";
    for (int i = 0; i < newsize; i++)
        cout << "{" << triparray[0][i] << "," << triparray[2][i] << "}, ";

    cout << "\n\n";
    for (int i = 0; i < newsize; i++)
        cout << "(" << triparray[0][i] << ";" << triparray[2][i] << ") ";


                              for (int i = 0; i < 2; i++)//Удаление sdarray[2][newsize]
                                  delete[] sdarray[i];
                              delete[] sdarray;
                              sdarray = 0;

                              for (int i = 0; i < 3; i++)//Удаление triparray[3][newsize]
                                  delete[] triparray[i];
                              delete[] triparray;
                              triparray = 0;
    return 0;
}